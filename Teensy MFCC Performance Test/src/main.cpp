#include <Arduino.h>
#include <Audio.h>  //Teensy Audio library
#include <OpenAudio_ArduinoLibrary.h> //Teensy Audio library extension for 32-bit FP FFT calculations
//#include <c_speech_features.h>
//#include <mfcc.cc>
#include <c_speech_features.c>

#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>


// Create the Audio components.  These should be created in the
// order data flows, inputs/sources -> processing -> outputs
//
//AudioInputI2S          audioInput;         // Input of audio shield, not installed but library needs an input and output in order to work(Sd card not valid input)
//AudioSynthWaveformSine sinewave;
AudioPlaySdWav         playSdWav;
//AudioAnalyzeFFT1024    myFFT;
//AudioOutputI2S         audioOutput;        // Output for audio shield, not installed in this case but library needs it

//32 bit floating point components from additional library
AudioInputI2S_F32      audioInput;
AudioOutputI2S_F32     audioOutput;
AudioAnalyzeFFT1024_F32 myFFT;          //32 bit floating point Fourier transform
AudioSynthWaveformSine_F32 sinewave;

AudioConvert_I16toF32 int2float;        //Retarded conversion needed because AudioPlaySdWav ouputs a 16 bit integer, for testing this is fine. But will likely not give accurate FFT results

//AudioConnection patchCord1(playSdWav, 0, int2float, 0); //Converts the 16 bit wav file into 32 bit float
//AudioConnection_F32 patchCord2(int2float, 0, myFFT, 0); //Calculate fourier transform

AudioConnection_F32 patchCord2(sinewave, 0, myFFT, 0);

// Connect either the live input or synthesized sine wave
//AudioConnection patchCord1(sinewave, 0, myFFT, 0);
//AudioConnection patchCord1(playSdWav, 0, myFFT, 0);

//AudioControlSGTL5000 audioShield; //Maybe needed to improve stability, allong with the enable in the setup

// Use these with the Teensy 3.5 & 3.6 SD card
#define SDCARD_CS_PIN    BUILTIN_SDCARD
#define SDCARD_MOSI_PIN  11  // not actually used
#define SDCARD_SCK_PIN   13  // not actually used

int cycleCounter;

void setup() {
  // Audio connections require memory to work.  For more
  // detailed information, see the MemoryAndCpuUsage example
  AudioMemory(20);
  AudioMemory_F32(50);  //Memory that is reserved for the Floating point audio functions

  Serial.begin(9600);
  // Enable the audio shield and set the output volume.
  //audioShield.enable(); //Maybe needed to improve stability

  // Configure the window algorithm to use
  //myFFT.windowFunction(AudioWindowHanning1024);
  myFFT.windowFunction(AudioWindowHanning1024);

  myFFT.setNAverage(1);

  myFFT.setOutputType(FFT_DBFS);

  // Create a synthetic sine wave, for testing
  // To use this, edit the connections above
  sinewave.amplitude(0.8f);
  sinewave.frequency(1034.007f);

  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);
  if (!(SD.begin(SDCARD_CS_PIN))) {
    while (1) {
      Serial.println("Unable to access the SD card");
      delay(500);
    }
  }
  Serial.println("Start playing");
  playSdWav.play("SDTEST1.WAV");
  delay(10); // wait for library to parse WAV info
}

void loop() {
  //Serial.println("Test");
  float n = 0;
  int i;
  if (myFFT.available()) {
    // each time new FFT data is available
    // print it all to the Arduino Serial Monitor
    Serial.print("FFT: ");
    //myFFT.processorUsageMaxReset();
    for (i=0; i<512; i++) {
      n = myFFT.read(i);
      //Serial.println(myFFT.processorUsageMax());
      Serial.println(n);
      if (n >= 0.01) {
        //Serial.print(n);
        //Serial.print(" ");
      } else {
        //Serial.print("  -  "); // don't print "0.00"
      }
    }
    Serial.print("Max processor usage: ");
    Serial.print(myFFT.processorUsageMax());
    Serial.print("\tMax memory usage: ");
    Serial.print(AudioMemoryUsageMax_F32());
    Serial.print("\t Cycle: ");
    Serial.print(cycleCounter);
    cycleCounter++;
    Serial.println();
    delay(10000);
  }
  else {
    //Serial.println("Niet availlable");
    //delay(500);
  }
}