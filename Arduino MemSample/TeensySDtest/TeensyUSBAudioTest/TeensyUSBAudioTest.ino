/*
 * USB_Audio_Testing
 * 
 * Created: Chip Audette, October 2016, openaudio.blogspot.com
 *          
 * Purpose: Use the "USB Audio" source and sink from the Teensy Audio 
 *          Library.  Can we get audio into and out of the Teensy?
 *          
 * License: MIT License.  Use at your own risk.
 */
#include "OpenAudio_ArduinoLibrary.h"
#include "AudioStream_F32.h"
//#include "USB_Audio_F32.h"
#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioInputUSB        audioInUSB1;    //xy=205,234
AudioEffectGain_F32      gain1;          //xy=373.00000381469727,303.00000381469727
AudioOutputI2S_F32       audioOutI2S1;   //xy=540.0000076293945,299.00000381469727
AudioOutputUSB       audioOutUSB1;   //xy=547,234
AudioConnection_F32          patchCord1(audioInUSB1, 0, audioOutUSB1, 0);
AudioConnection_F32          patchCord2(audioInUSB1, 1, gain1, 0);
AudioConnection_F32          patchCord3(audioInUSB1, 1, audioOutI2S1, 0);
AudioConnection_F32          patchCord4(gain1, 0, audioOutI2S1, 1);
AudioConnection_F32          patchCord5(gain1, 0, audioOutUSB1, 1);
AudioControlSGTL5000     sgtl5000_1;     //xy=451,107
// GUItool: end automatically generated code


void setup() {
  delay(250);  AudioMemory(50);  delay(250);

  // Enable the audio shield and set the output volume.
  sgtl5000_1.enable();
  sgtl5000_1.inputSelect(AUDIO_INPUT_LINEIN);
  sgtl5000_1.volume(0.5); //headphone volume
  gain1.setGain_dB(0);
}

void loop() {
   Serial.println("Hey commandlin");
   delay(20);
}
