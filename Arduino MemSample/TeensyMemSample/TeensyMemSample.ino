// Advanced Microcontroller-based Audio Workshop
//
// http://www.pjrc.com/store/audio_tutorial_kit.html
// https://hackaday.io/project/8292-microcontroller-audio-workshop-had-supercon-2015
// 
// Part 2-3: Playing Samples

// WAV files converted to code by wav2sketch
#include "AudioSampleHotblowsegment.h"        
#include <Bounce.h>

elapsedMillis blowAudioTiming;
///////////////////////////////////
// copy the Design Tool code here
///////////////////////////////////

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioPlayMemory          playMem1;       //xy=306,437
AudioMixer4              mixer1;         //xy=491,514
AudioOutputI2S           i2s1;           //xy=674,482
AudioAnalyzeFFT1024      myFFT;      //xy=742,557
AudioConnection          patchCord1(playMem1, 0, mixer1, 0);
AudioConnection          patchCord2(mixer1, myFFT);
AudioConnection          patchCord3(mixer1, 0, i2s1, 0);
AudioConnection          patchCord4(mixer1, 0, i2s1, 1);
// GUItool: end automatically generated code


void setup() {
  AudioMemory(10);
  //sgtl5000_1.enable();
  //sgtl5000_1.volume(0.5);
  mixer1.gain(0, 0.5);
  myFFT.windowFunction(AudioWindowHanning1024);
}

void loop() {
if(blowAudioTiming < 100){

  Serial.println("Playing audio file");
  playMem1.play(AudioSampleHotblowsegment);
}  
else if (myFFT.available()) {
    // each time new FFT data is available
    // print it all to the Arduino Serial Monitor
    Serial.print("FFT: ");
    for (int i=0; i<40; i++) {
      float n = myFFT.read(i);
      if (n >= 0.01) {
        Serial.print(n);
        Serial.print(" ");
      } else {
        Serial.print("  -  "); // don't print "0.00"
      }
    }
    Serial.println();
  }
else if (blowAudioTiming >= 2500) {
  playMem1.stop();
  for (int i=0; i<10; i++){
    Serial.println("");
  }
  Serial.println("5 seconds have elapsed");
  Serial.println("Restarting audio file");
  blowAudioTiming = 0;
}
}
